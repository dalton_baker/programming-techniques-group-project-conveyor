/*************************************************************************//**
 * @file
 * @brief The unsortedDouble linked list class & all of it's functions
 ****************************************************************************/
#include <iostream>
#include <string>
#include <functional>
using std::nothrow;
using std::ostream;
using std::string;
using std::function;


#ifndef UNSORTEDDOUBLE__H
#define UNSORTEDDOUBLE__H

/*!
* @brief The unsortedDouble class, this is a template double linked list class
*/
template <typename TY>
class unsortedDouble
{
    public:
        unsortedDouble();
        unsortedDouble ( const unsortedDouble<TY> &l );
        ~unsortedDouble();
        
        void clear();
        void print ( ostream &out, bool forward = true,
                     string seperator = ", " );
        bool insert ( TY val, int pos = 1 );
        bool remove ( int pos = 1 );
        bool removeVal ( TY val );
        bool rfind ( TY val );
        bool find ( TY val );
        
        bool isEmpty( );
        int size();
        bool push_back ( TY val );
        bool push_front ( TY val );
        bool pop_back ( TY &val );
        bool pop_front ( TY &val );
        
        void swap ( unsortedDouble<TY> &l );
        int countIf ( function<bool ( TY ) > cond );
        int retrieve ( TY val );
        int rretrieve ( TY val );
        bool retrieve ( TY &item, int position );
        
    private:
        /*!
        * @brief Structure to hold the data of a single node
        */
        struct node
        {
            TY item;    /*!< The item in the node*/
            node *next; /*!< A pointer to the next node*/
            node *last; /*!< A pointer to the previous node*/
        };
        node *headptr;  /*!< A pointer to the begining of the list*/
        node *tailptr;  /*!< A pointer to the end of the list*/
};

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This is a copy constructor, it makes a copy of a linked list when creating a
 * new linked list.
 *
 * @param[in] l - the linked list to be copied
 *
 *****************************************************************************/
template <typename TY>
unsortedDouble<TY>::unsortedDouble ( const unsortedDouble<TY> &l )
{
    //create a temporary pointer to read the data
    node *copy = l.headptr,  *dest = nullptr;
    
    //handle the first item in the list
    if ( copy == nullptr )
    {
        headptr = nullptr;
        tailptr = nullptr;
        return;
    }
    
    if ( const_cast <unsortedDouble &> ( l ).size() == 0 )
    {
        headptr = nullptr;
        tailptr = nullptr;
        return;
    }
    
    //create first node and fill in info
    headptr = new ( nothrow ) node;
    headptr->item = copy->item;
    headptr->next = nullptr;
    headptr->last = nullptr;
    tailptr = headptr;
    copy = copy->next;
    dest = headptr;
    
    //copy each item into the new list
    while ( copy != nullptr )
    {
        //create new node
        dest->next = new ( nothrow ) node;
        
        //link node to surounding nodes
        dest->next->last = dest;
        dest = dest->next;
        dest->next = nullptr;
        tailptr = dest;
        
        //copy data from orig list & move ptr down
        dest->item = copy->item;
        copy =  copy->next;
    }
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This is a copy constructor, it createsd an empty linked list
 *
 *****************************************************************************/
template <typename TY>
unsortedDouble<TY>::unsortedDouble()
{
    //make new empty list
    headptr = nullptr;
    tailptr = nullptr;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This is a destructor, it frees all dynamic memory and deletes the list
 *
 *****************************************************************************/
template <typename TY>
unsortedDouble<TY>::~unsortedDouble()
{
    //clear the list
    clear();
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This function completely deletes a linked list & frees all dynamic memory
 *
 *****************************************************************************/
template <typename TY>
void unsortedDouble<TY>::clear()
{
    //check if the list is already empty
    if ( headptr == nullptr && tailptr == nullptr )
    {
        return;
    }
    
    node *temp = headptr;
    node *next = headptr->next;
    
    //delete each item in list
    while ( next != nullptr )
    {
        delete temp;
        temp = next;
        next = next->next;
    }
    
    delete temp;
    
    //set tailptr & headptr to nullptr
    headptr = nullptr;
    tailptr = nullptr;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This is a print function, it prints the list forward or backward
 * to the chosen place.
 *
 * @param[in] out - the place to print to
 *
 * @param[in] forward - a bool for printing forward
 *
 * @param[in] seperator - the object to print in between each node in the list
 *
 *****************************************************************************/
template <typename TY>
void unsortedDouble<TY>::print ( ostream &out, bool forward,
                                 string seperator )
{
    node *printer;
    
    //check if list is empty
    if ( headptr == nullptr )
    {
        return;
    }
    
    //print list forward
    if ( forward )
    {
        printer = headptr;
        
        while ( printer->next != nullptr )
        {
            out << printer->item << seperator;
            printer = printer->next;
        }
        
        out << printer->item;
    }
    
    //print list backward
    if ( !forward )
    {
        printer = tailptr;
        
        while ( printer->last != nullptr )
        {
            out << printer->item << seperator;
            printer = printer->last;
        }
        
        out << printer->item;
    }
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This function will insert an item at a particular location in the list
 *
 * @param[in] val - item to be added to the list
 *
 * @param[in] pos - the position to add the item to
 *
 * @returns true - succesfuly added item to list
 *
 * @returns false - failed to add item to list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::insert ( TY val, int pos )
{
    //check the positions validity
    if ( pos > ( size() + 1 ) || pos < 1 )
    {
        return false;
    }
    
    //create new item
    node *insertion = new ( nothrow ) node;
    insertion->item = val;
    insertion->last = nullptr;
    insertion->next = nullptr;
    
    //first item in list
    if ( headptr == nullptr )
    {
        headptr = insertion;
        tailptr = insertion;
        return true;
    }
    
    node *prevPos = headptr;
    node *position = headptr;
    
    //locate the position
    for ( int i = 1; i < pos && position != nullptr; i++ )
    {
        prevPos = position;
        position = position->next;
    }
    
    //inserting at begining
    if ( pos == 1 )
    {
        insertion->next = headptr;
        headptr = insertion;
    }
    
    else
    {
        insertion->next = prevPos->next;
        prevPos->next = insertion;
    }
    
    //inserting at end
    if ( position == nullptr )
    {
        insertion->last = tailptr;
        tailptr = insertion;
    }
    
    else
    {
        insertion->last = position->last;
        position->last = insertion;
    }
    
    return true;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will delete an item ata  particular location from the list
 *
 * @param[in] pos - the position of the item to remove
 *
 * @returns true - succesfuly removed item from the list
 *
 * @returns false - failed to remove item from the list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::remove ( int pos )
{
    //check for a position out of bounds & empty list
    if ( pos < 1 || pos > size() || headptr == nullptr )
    {
        return false;
    }
    
    //temporary variables
    node *remove = headptr;
    node *prev = headptr;
    int count = 1;
    
    //special case for final item
    if ( size() == 1 )
    {
        delete remove;
        headptr = nullptr;
        tailptr = nullptr;
        return true;
    }
    
    //special case for first item in list
    if ( pos == 1 )
    {
        remove->next->last = nullptr;
        headptr = remove->next;
        delete remove;
        return true;
    }
    
    //special case for last item in list
    if ( pos == size() )
    {
        remove = tailptr;
        remove->last->next = nullptr;
        tailptr = remove->last;
        delete remove;
        return true;
    }
    
    //seek to the position
    while ( count != pos )
    {
        prev = remove;
        remove = remove->next;
        count++;
    }
    
    //remove from middle
    remove->next->last = prev;
    prev->next = remove->next;
    delete remove;
    return true;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will delete a particular item from the list.
 *
 * @param[in] val - the item to be removed from the list
 *
 * @returns true - succesfuly removed item from the list
 *
 * @returns false - failed to remove item from the list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::removeVal ( TY val )
{
    //search the string for the value
    if ( !find ( val ) )
    {
        return false;
    }
    
    //temporary variables
    node  *posRead = headptr;
    int pos = 1;
    
    //find the position of the value
    while ( posRead->item != val )
    {
        posRead = posRead->next;
        pos++;
    }
    
    //remove node at the position and return the bool
    return remove ( pos );
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will search for an item in the list starting at the end
 *
 * @param[in] val - the value being searched for
 *
 * @returns true - the item was in the list
 *
 * @returns false - the item was not in the list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::rfind ( TY val )
{
    node *rfind = tailptr;
    
    //check if the list is empty
    if ( tailptr == nullptr )
    {
        return false;
    }
    
    //read each item in the list & compare it
    while ( rfind->item != val && rfind->last != nullptr )
    {
        rfind = rfind->last;
    }
    
    //compare found value
    if ( rfind->item == val )
    {
        return true;
    }
    
    //return false if not found
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will search for an item in the list starting at the begining
 *
 * @param[in] val - the value being searched for
 *
 * @returns true - the item was in the list
 *
 * @returns false - the item was not in the list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::find ( TY val )
{
    node *find = headptr;
    
    //check if the list is empty
    if ( headptr == nullptr )
    {
        return false;
    }
    
    //read each item in the list & compare it
    while ( find->item != val && find->next != nullptr )
    {
        find = find->next;
    }
    
    //compare found value
    if ( find->item == val )
    {
        return true;
    }
    
    //return false if not found
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This function will tell the user if the list is empty
 *
 * @returns true - the list is empty
 *
 * @returns false - the list is not empty
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::isEmpty()
{
    //check if headptr and tailptr are empty
    if ( headptr == nullptr && tailptr == nullptr )
    {
        return true;
    }
    
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This wil return the size of the list to the user
 *
 * @returns size - the size of the list
 *
 *****************************************************************************/
template <typename TY>
int unsortedDouble<TY>::size()
{
    //create a variable to hold the size
    int size = 0;
    
    node *count = headptr;
    
    //count the number of items in the list
    while ( count != nullptr )
    {
        count = count->next;
        size += 1;
    }
    
    //return the size of the list
    return size;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will add an item to the end of the list
 *
 * @param[in] val - the item to be added to the list
 *
 * @returns true - succesfuly added item to list
 *
 * @returns false - failed to add item to list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::push_back ( TY val )
{
    //create a new node & check if it exists
    node *newNode = new ( nothrow ) node;
    
    if ( newNode == nullptr )
    {
        return false;
    }
    
    //fill node with value
    newNode->item = val;
    newNode->next = nullptr;
    newNode->last = nullptr;
    
    //insert if the list is empty
    if ( isEmpty() )
    {
        tailptr = newNode;
        headptr = newNode;
        return true;
    }
    
    //insert node if list is not empty
    else if ( !isEmpty() )
    {
        tailptr->next = newNode;
        newNode->last = tailptr;
        tailptr = newNode;
        return true;
    }
    
    //return false if something went wrong
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will add an item to the front of the list
 *
 * @param[in] val - the item to be added to the list
 *
 * @returns true - succesfuly added item to list
 *
 * @returns false - failed to add item to list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::push_front ( TY val )
{
    //create a new node & check if it exists
    node *newNode = new ( nothrow ) node;
    
    if ( newNode == nullptr )
    {
        return false;
    }
    
    //fill node with value
    newNode->item = val;
    newNode->next = nullptr;
    newNode->last = nullptr;
    
    //insert if the list is empty
    if ( isEmpty() )
    {
        headptr = newNode;
        tailptr = newNode;
        return true;
    }
    
    //insert node if list is not empty
    else if ( !isEmpty() )
    {
        headptr->last = newNode;
        newNode->next = headptr;
        headptr = newNode;
        return true;
    }
    
    //return false if something went wrong
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will remove an item from the end of the list & give you the items value
 *
 * @param[in] val - the value of the item, returns the value to user
 *
 * @returns true - succesfuly removed item from list
 *
 * @returns false - failed to remove item from list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::pop_back ( TY &val )
{
    //create temp variable to point to the removed item
    node *remove = tailptr;
    
    //check if the list is empty
    if ( isEmpty() )
    {
        return false;
    }
    
    //remove item if it is the last in the list
    if ( size() == 1 )
    {
        headptr = nullptr;
        tailptr = nullptr;
        val = remove->item;
        delete remove;
        return true;
    }
    
    //remove item if it isn't the last in the list
    else if ( size() > 1 )
    {
        tailptr = tailptr->last;
        tailptr->next = nullptr;
        val = remove->item;
        delete remove;
        return true;
    }
    
    //return false if something went wrong
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * Removes an item from the front of the list & give you the items value
 *
 * @param[in] val - the value of the item, returns the value to user
 *
 * @returns true - succesfuly removed item from list
 *
 * @returns false - failed to remove item from list
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::pop_front ( TY &val )
{
    //create temp variable to point to the removed item
    node *remove = headptr;
    
    //check if the list is empty
    if ( isEmpty() )
    {
        return false;
    }
    
    //remove item if it is the last in the list
    if ( size() == 1 )
    {
        headptr = nullptr;
        tailptr = nullptr;
        val = remove->item;
        delete remove;
        return true;
    }
    
    //remove item if it isn't the last in the list
    else if ( size() > 1 )
    {
        headptr = headptr->next;
        headptr->last = nullptr;
        val = remove->item;
        delete remove;
        return true;
    }
    
    //return false if something went wrong
    return false;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will swap the contents of two different linked lists
 *
 * @param[in] l - the linked list to be swapped with
 *
 *****************************************************************************/
template <typename TY>
void unsortedDouble<TY>::swap ( unsortedDouble<TY> &l )
{
    //hold the headptr and tailptr values
    node *temphead = headptr;
    node *temptail = tailptr;
    
    //swap the headptr's of each list
    headptr = l.headptr;
    l.headptr = temphead;
    
    //swap tailptr's of each list
    tailptr = l.tailptr;
    l.tailptr = temptail;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will return the position of a particular item in the list, starting at
 * the front
 *
 * @param[in] val - the value being looked for
 *
 * @returns count - the position of an item in the list
 *
 *****************************************************************************/
template <typename TY>
int unsortedDouble<TY>::retrieve ( TY val )
{
    node *find = headptr;
    int count = 1;
    
    //walk through the list checking each item
    while ( find != nullptr && find->item != val )
    {
        find = find->next;
        count += 1;
    }
    
    //check if find is nullptr
    if ( find == nullptr )
    {
        return 0;
    }
    
    //return count if the item was found
    return count;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will return the position of a particular item in the list, starting at
 * the end
 *
 * @param[in] val - the value being looked for
 *
 * @returns count - the position of an item in the list
 *
 *****************************************************************************/
template <typename TY>
int unsortedDouble<TY>::rretrieve ( TY val )
{
    node *find = tailptr;
    int count = size();
    
    //walk through the list backwards checking each item
    while ( find != nullptr && find->item != val )
    {
        find = find->last;
        count -= 1;
    }
    
    //check if find is nullptr
    if ( find == nullptr )
    {
        return 0;
    }
    
    //return count if the item was found
    return count;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will retrieve an item at a particular location
 *
 * @param[in] item - the value of the item, returns the value to user
 *
 * @param[in] position - the position of the item being retrieved
 *
 * @returns true - successfuly retrieved item
 *
 * @returns false - could not retrieve item
 *
 *****************************************************************************/
template <typename TY>
bool unsortedDouble<TY>::retrieve ( TY &item, int position )
{
    //check the position provided
    if ( position < 1 || position > size() )
    {
        return false;
    }
    
    //walk through the list to get the item
    node *ret = headptr;
    
    for ( int i = 1; i < position; i++ )
    {
        ret = ret->next;
    }
    
    //get the item and return true
    item = ret->item;
    return true;
}

/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This will return a count of all items in the list that meet a certain
 * requirement.
 *
 * @param[in] cond - the function used as a requirement
 *
 * @returns count - the number of items meeting the requirement
 *
 *****************************************************************************/
template <typename TY>
int unsortedDouble<TY>::countIf ( function<bool ( TY ) > cond )
{
    int count = 0;
    node *theIf = headptr;
    
    //walk through the list and find each "if"
    while ( theIf != nullptr )
    {
        if ( cond ( theIf->item ) )
        {
            count += 1;
        }
        
        theIf = theIf->next;
    }
    
    //return the number found
    return count;
}
#endif
