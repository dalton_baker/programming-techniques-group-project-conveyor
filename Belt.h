/*************************************************************************//**
 * @file
 * @brief The conveyorBelt class header file
 ****************************************************************************/
#ifndef __BELT__H__
#define __BELT__H__
#include "unsortedDouble.h"

/*!
* @brief Structure to hold the data for box on the conveyor belt
*/
struct box
{
    int boxNumber;  /*!< The number of the box in the sequence*/
    int width;      /*!< The width of the box*/
    int depth;      /*!< The depth of the box*/
    int height;     /*!< The height of the box*/
};

/*!
* @brief A class designed to hold and manipulate the data of a conveyor belt
*/
class conveyorBelt
{
    public:
    
        conveyorBelt ( char beltLetter, int length, int width );
        conveyorBelt ( conveyorBelt &oldBelt );
        ~conveyorBelt();
        
        bool addBox ( box package );
        bool removeBox ( box &package );
        void rotatePackage ( box &package );
        bool needsRemoved ( void );
        int returnWidth ( void );
        void swap ( conveyorBelt &swapBelt );
        char beltNum ( void );
        
    private:
        unsortedDouble<box> theBelt;    /*!< The contents of the belt*/
        int theLength;                  /*!< The length of the belt*/
        int theWidth;                   /*!< The width of the belt*/
        int space;                      /*!< The space used on the belt*/
        char letter;                    /*!< The letter of the belt*/
        
};
#endif
