/*************************************************************************//**
 * @file
 * @brief Start of the program, only contains main.
 ****************************************************************************/

/*************************************************************************//**
 * @file
 *
 * @mainpage Program 4 - Conveyor Belts
 *
 * @section course_section Course Information
 *
 * @authors Makiya, Haley, & Dalton Baker
 *
 * @date 4 / 27 / 2018
 *
 * @par Professor:
 *         Roger Schrader
 *
 * @par Course:
 *         CSC 215 - Section M001 - 11:00AM
 *
 * @par Location:
 *         McLaury - Room 205E
 *
 * @section program_section Program Information
 *
 * This program is used to sort boxes onto conveyor belts and see if the fall
 * off the end. This is accomplished by using cammand line agruments to load
 * in test simulations for the box and conveyor belt data. The program will
 * then read in the box dimensions and load them into a list. The program will
 * continue to add boxes and will, check to see if a box falls off the end of
 * the conveyer belt. The test simulation will countinue untill there is a
 * -1 -1 -1 for the box dimensions. The test simulation will continue to read
 * in until there is 0 0 0 then the program will output its data to a file and
 * exit.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      Compile like any normal program
 *
 * @par Usage:
 * You must provide 2 arguments to the program. The first will be a file
 * and the second will be an integer. The file must contain simulations and the
 * integer will be the length of the conveyor belt.
   @verbatim
   c:\> prog4.exe example.sim 40
   c:\> prog4.exe c:\bin\example2.sim 35
   d:\> c:\bin\prog4.exe exmaple3.sim 25
   @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug Nothing known currently
 *
 * @todo Everything needs to be done
 *
 * @par Modifications and Development Timeline:
 * <a href="https://gitlab.mcs.sdsmt.edu/csc215s18prog4/team03/commits/master">
 * Click Here </a> for a list of all contributions made to this program.
 *
 ****************************************************************************/

#include "Belt.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>

using namespace std;

unsortedDouble<int> getFileData ( string fileName );
string runSim ( unsortedDouble<int> &simData,
                const int beltLength );
box getBox ( int boxNum, int valA, int valB, int valC );
void sortBelts ( conveyorBelt &beltA, conveyorBelt &beltB,
                 conveyorBelt &beltC );
string checkBoxFalls ( conveyorBelt &checkBelt );
void writeDateFile ( string data, string fileName );
string addBox ( box theBox, conveyorBelt &beltA, conveyorBelt &beltB,
                conveyorBelt &beltC );


/**************************************************************************//**
 * @author Makiya Crochiere, Haley Linnig, & Dalton Baker
 *
 * @par Description:
 * This is the main function, the program starts here.
 *
 * @param[in]  argc - The number of arguments provided to the program
 * @param[in]  argv - A 2D array containing the arguments provided
 *                      to the program.
 *
 * @returns 0 The program ran with no errors.
 * @returns 1 Something went wrong in the program.
 *
 *****************************************************************************/
int main ( int argc, char **argv )
{
    string simData;
    int beltLength, count;
    
    //Must have program name, input file, and length of belts
    if ( argc != 3 )
    {
        cout << "Invalid number of arguments" << endl;
        cout << "Usage: prog3.exe <boxSim_filename> <length of belt>" << endl;
        return 1;
    }
    
    //Goes through entirety of argv[2] and checks that it is a valid integer
    for ( count = 0; count < strlen ( argv[2] ); count++ )
    {
        if ( !isdigit ( argv[2][count] ) )
        {
            cout << "Belt length must be a valid integer." << endl;
            return 1;
        }
    }
    
    //Set belt lengths to third command line argument
    beltLength = atoi ( argv[2] );
    
    //Get all info out of a file
    unsortedDouble<int> sims = getFileData ( argv[1] );
    
    //run the simulations
    simData = runSim ( sims, beltLength );
    
    //write the siumulation data to a file
    writeDateFile ( simData, "run.out" );
    
    return 0;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will retrieve all the data from a file
 *
 * @param[in] fileName - the name of the file to get the data from
 *
 * @returns fileCont - a vector containing all of the data
 *
 *****************************************************************************/
unsortedDouble<int> getFileData ( string fileName )
{
    unsortedDouble<int> fileCont;
    int temp;
    
    //open file and check if it opened
    ifstream simFile ( fileName.c_str() );
    
    if ( !simFile.is_open() )
    {
        cout << "Input file \"" << fileName;
        cout << "\" could not be opened" << endl;
        exit ( 1 );
    }
    
    //read all file contents into the vector
    while ( simFile >> temp )
    {
        fileCont.push_front ( temp );
    }
    
    //close the file
    simFile.close();
    
    //return the vector
    return fileCont;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will run the simulations provided in the file and return
 * the results of the simulation.
 *
 * @param[in] simData - a vector containing all of the data
 *
 * @param[in] beltLength - the length of the belts
 *
 * @returns results - a string containing the results of the simulation
 *
 *****************************************************************************/
string runSim ( unsortedDouble<int> &simData, const int beltLength )
{
    //create a vector of stings to hold the results and a temp int
    string results;
    int valA, valB, valC, simCount = 1;
    
    //get first 3 values and check that the sim should run
    while ( simData.pop_back ( valA ) && simData.pop_back ( valB ) &&
            simData.pop_back ( valC ) && valA != 0 && valB != 0 && valC != 0 )
    {
        //create a temp int to represent the number of boxes
        int boxCount = 1;
        
        //output the simulation number
        results += "Simulation " + to_string ( simCount ) + ":\n";
        
        //set up and sort the conveyor belts
        conveyorBelt beltA ( 'A', beltLength, valA ),
                     beltB ( 'B', beltLength, valB ),
                     beltC ( 'C', beltLength, valC );
        sortBelts ( beltA, beltB, beltC );
        
        //check if the sim should end
        while ( simData.pop_back ( valA ) && simData.pop_back ( valB ) &&
                simData.pop_back ( valC ) &&
                valA != -1 && valB != -1 && valC != -1 )
        {
            //create the box
            box theBox = getBox ( boxCount, valA, valB, valC );
            
            //try to add the box to a conveyor belt
            results += addBox ( theBox, beltA, beltB, beltC );
            
            //increment the box count
            boxCount++;
        }
        
        //output a new line and increment the simulation number
        results += "\n";
        simCount++;
    }
    
    //return the results
    return results;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will sort the sides of a box and create a structure with that
 * data
 *
 * @param[in] boxNum - the number of the box
 *
 * @param[in] valA - the first side of the box
 *
 * @param[in] valB - the second side of the box
 *
 * @param[in] valC - the third side of the box
 *
 * @returns theBox - the box structure containg the box
 *
 *****************************************************************************/
box getBox ( int boxNum, int valA, int valB, int valC  )
{
    //create some temp variables and fill them
    vector<int> temp = { valA, valB, valC };
    
    //sort the data
    sort ( temp.begin(), temp.end() );
    
    //insert the data into the box
    box theBox{ boxNum, temp[0], temp[1], temp[2] };
    
    //return the box
    return theBox;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will add a box to a conveyor, then it will check if a box
 * falls off that conveyor.
 *
 * @param[in] beltA - the first belt, will end up as the thinest belt
 *
 * @param[in] beltB - the first belt, will end up as the middle belt
 *
 * @param[in] beltC - the first belt, will end up as the widest belt
 *
 *****************************************************************************/
void sortBelts ( conveyorBelt &beltA, conveyorBelt &beltB,
                 conveyorBelt &beltC )
{
    //walk through the belts 2 times
    for ( int i = 0; i < 2; i++ )
    {
        //check the first 2 positions
        if ( beltA.returnWidth() > beltB.returnWidth() )
        {
            beltA.swap ( beltB );
        }
        
        //check the second set of positions
        if ( beltB.returnWidth() > beltC.returnWidth() )
        {
            beltB.swap ( beltC );
        }
    }
    
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will check if any boxes are falling off the belt, and return
 * information about the boxes that do fall
 *
 * @param[in] checkBelt - the belt being checked
 *
 * @returns info - a string containing the results of the simulation
 *
 *****************************************************************************/
string checkBoxFalls ( conveyorBelt &checkBelt )
{
    //create a string to hold the info
    string info = "";
    //create a box to hold the data of a falling box
    box fell;
    
    //check if boxes are falling
    while ( checkBelt.needsRemoved() )
    {
        //remove the box
        checkBelt.removeBox ( fell );
        //add the box data to the info
        info += "   Box " + to_string ( fell.boxNumber ) + " fell of conveyor "
                + checkBelt.beltNum() + "\n";
    }
    
    //return the info
    return info;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will write the results of the simulation to a file
 *
 * @param[in] data - the data from the simulation
 *
 * @param[in] fileName - the name of the output file
 *
 *****************************************************************************/
void writeDateFile ( string data, string fileName )
{
    //open file and check if it opened
    ofstream saveFile ( fileName.c_str() );
    
    if ( !saveFile.is_open() )
    {
        cout << "output file \"" << fileName;
        cout << "\" could not be opened" << endl;
        exit ( 1 );
    }
    
    //output data
    saveFile << data;
    
    //close the file
    saveFile.close();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will write the results of the simulation to a file
 *
 * @param[in] theBox - the box being added to a conveyor
 *
 * @param[in] beltA - the first conveyor belt 
 *
 * @param[in] beltB - the second conveyor belt
 *
 * @param[in] beltC - the third conveyor belt
 *
 * @returns string - a string containing the results of adding the box
 *
 *****************************************************************************/
string addBox ( box theBox, conveyorBelt &beltA, conveyorBelt &beltB,
                conveyorBelt &beltC )
{
    //try to add to belt A
    if ( beltA.addBox ( theBox ) )
    {
        //check if a box fell
        return checkBoxFalls ( beltA );
    }
    
    //try to add to belt B
    else if ( beltB.addBox ( theBox ) )
    {
        //check if a box fell
        return checkBoxFalls ( beltB );
    }
    
    //try to add to belt C
    else if ( beltC.addBox ( theBox ) )
    {
        //check if a box fell
        return checkBoxFalls ( beltC );
    }
    
    //the box was to big
    return "   Box " + to_string ( theBox.boxNumber )
           + " did not fit on any conveyor\n";
}