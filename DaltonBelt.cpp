/*************************************************************************//**
 * @file
 * @brief Dalton's Belt class functions
 ****************************************************************************/
#include "Belt.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This constructor will initiate a new belt
 *
 * @param[in] beltLetter - the name of the belt
 *
 * @param[in] length - the length of the belt
 *
 * @param[in] width - the width of the belt
 *
 *****************************************************************************/
conveyorBelt::conveyorBelt ( char beltLetter, int length, int width )
{
    //set the provided variables
    theLength = length;
    theWidth = width;
    letter = beltLetter;
    
    //make sure everything else is clear
    space = 0;
    theBelt.clear();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This destructor will delete dynamic memory
 *
 *****************************************************************************/
conveyorBelt::~conveyorBelt()
{
    theBelt.clear();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will add a box to the belt
 *
 * @param[in] package - the package to be added to the belt
 *
 * @returns true - succesfuly added package
 *
 * @returns false - failed to add package
 *
 *****************************************************************************/
bool conveyorBelt::addBox ( box package )
{
    //check if thge box will fit
    if ( package.width > theWidth )
    {
        return false;
    }
    
    //rotate box to proper orientation
    rotatePackage ( package );
    
    //attempt to add the box to the belt
    if ( theBelt.push_back ( package ) )
    {
        //increase the space used on the belt
        space += package.depth;
        return true;
    }
    
    //return false if the add failed
    return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will remove a box from the belt
 *
 * @param[in] package - the package being removed from the list
 *
 * @returns true - succesfuly removed package
 *
 * @returns false - failed to remove package
 *
 *****************************************************************************/
bool conveyorBelt::removeBox ( box &package )
{
    //remove the box from the belt
    if ( theBelt.pop_front ( package ) )
    {
        //decrement the amount of space used
        space -= package.depth;
        return true;
    }
    
    return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will remove a box from the belt
 *
 * @param[in] package - the package that is being removed
 *
 *****************************************************************************/
void conveyorBelt::rotatePackage ( box &package )
{
    //check if the package needs rotated
    if ( package.depth <= theWidth )
    {
        //swap the width and depth
        int temp = package.width;
        package.width = package.depth;
        package.depth = temp;
    }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will check if a box will fall off the end
 *
 * @returns true - a box will fall
 *
 * @returns false - no box will fall
 *
 *****************************************************************************/
bool conveyorBelt::needsRemoved()
{
    //look at the box at the front of the line
    box temp;
    theBelt.retrieve ( temp, 1 );
    
    //check if the box will fall of the end
    return ( space - theLength ) > ( ( temp.depth / 2.0 ) );
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will give you the width of the belt.
 *
 * @returns theWidth - the width of the belt
 *
 *****************************************************************************/
int conveyorBelt::returnWidth ( void )
{
    return theWidth;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will give you the identifying letter of the belt.
 *
 * @returns letter - the letter of the belt
 *
 *****************************************************************************/
char conveyorBelt::beltNum ( void )
{
    return letter;
}