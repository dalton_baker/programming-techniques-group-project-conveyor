/*************************************************************************//**
 * @file
 * @brief Haley's Belt class functions
 ****************************************************************************/
#include "Belt.h"

/**************************************************************************//**
 * @author Haley Linnig
 *
 * @par Description:
 * This is the copy constructor for the conveyorBelt class. It will create
 * an item of the conveyorBelt class from another conveyorBelt class item;
 * create one conveyorBelt class item and copying another into it.
 *
 * @param[in] oldBelt - conveyorBelt item that will be copied into new item
 *
 *****************************************************************************/

conveyorBelt::conveyorBelt ( conveyorBelt &oldBelt )
{
    //Copies theBelt from the other theBelt
    unsortedDouble<box> theBelt ( oldBelt.theBelt );
    
    //Creates conveyor belt length, width, space & letter
    theLength = oldBelt.theLength;
    theWidth = oldBelt.theWidth;
    space = oldBelt.space;
    letter = oldBelt.letter;
}

/**************************************************************************//**
 * @author Haley Linnig
 *
 * @par Description:
 * This function will swap two conveyorBelt items. It will call the copy
 * constructor in order to create a temporary variable to hold swapBelt.
 * This function will then swap each part of the conveyorBelt class items using
 * the unsortedDouble sort for the unsortedDouble item within the class.
 *
 * @param[in] swapBelt - conveyorBelt item to be swapped
 *
 * @returns  - Will return if the two belts were swapped
 *
 *****************************************************************************/

void conveyorBelt::swap ( conveyorBelt &swapBelt )
{
    //Creates temporary conveyorBelt of swapBelt
    conveyorBelt temp ( swapBelt );
    
    //Swap theBelt unsorted lists
    theBelt.swap ( swapBelt.theBelt );
    
    //Set swapBelt to conveyorBelt
    swapBelt.theLength = theLength;
    swapBelt.theWidth = theWidth;
    swapBelt.space = space;
    swapBelt.letter = letter;
    
    //Set conveyorBelt to temp (swapBelt)
    theLength = temp.theLength;
    theWidth = temp.theWidth;
    space = temp.space;
    letter = temp.letter;
    
    return;
}


